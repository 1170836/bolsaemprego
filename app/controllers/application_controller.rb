class ApplicationController < ActionController::Base 
    include SessionsHelper

private
  def logged_in_user
    unless logged_in?
      store_location
      flash[:danger] = "Por favor efectue o login na aplicação."
      redirect_to login_url
    end
  end

  def admin_user
    redirect_to(root_url) unless current_user.tipo == 'admin'
  end

  def original_url
    base_url + original_fullpath
  end

  def correct_user
    @user = User.find(params[:id])
    redirect_to(root_url) unless current_user?(@user) || admin_user?(current_user)
  end
end
