class BackofficeController < ApplicationController

    before_action :logged_in_user
    before_action :admin_user
    
    def show
    end

    def new
        @user = User.new
    end

    def home
        render 'backoffice/home'
    end
    
    def index_users
        @users = User.all.order(updated_at: :desc)
        @users = @users.tipo(params[:tipo]) if params[:tipo].present?
        @users = @users.estado(params[:ativo]) if params[:ativo].present?
        @users = @users.search(params[:search]) if params[:search].present?
        @users = @users.paginate(page: params[:page], per_page: 10)
    end

    def index_noticias
        @noticias = Noticia.all.order(updated_at: :desc)
        @noticias  = @noticias.titulo(params[:titulo]) if params[:titulo].present?
        @noticias = @noticias.paginate(page: params[:page], per_page: 10)
    end

    def show_noticia
        @noticia = Noticia.find(params[:id])
    end

    def show_users
        @user = User.find(params[:id])
    end

    def edit_users
        @user = User.find(params[:id])
    end

    def edit_user_password
        @user = User.find(params[:id])
    end

    def edit_noticias
        @noticia = Noticia.find(params[:id])
    end
end
