class NoticiasController < ApplicationController

    before_action :logged_in_user, only: [:edit, :update, :destroy]
    before_action :admin_user, only:[:edit, :update, :destroy]
    
    def new
        @noticia = Noticia.new
    end
    
    
    def create
        @noticia = Noticia.new(noticias_params)
        if @noticia.save
            flash[:success] = "Notícia criada com sucesso!"
            redirect_to backoffice_show_noticia_path(@noticia)
        else
            render 'new'
        end
    end

    def destroy
        Noticia.find(params[:id]).destroy
        flash[:success] = "Notícia apagada!"
        redirect_to backoffice_noticias_url
    end

    def update
        @noticia = Noticia.find(params[:id])
        if @noticia.update_attributes(noticias_params)
          flash[:success] = "Notícia actualizada!"
          redirect_to backoffice_show_noticia_path(@noticia)
        else
          render 'backoffice/edit_noticias'
        end
    end

    def show
        @noticia = Noticia.find(params[:id])
    end

    def index
        @noticias = Noticia.ativa.order(updated_at: :desc)
        @noticias = @noticias.paginate(page: params[:page], per_page: 8)
    end

    private
    def noticias_params
        params.require(:noticia).permit(:titulo, :data, :sumario, :descricao, :destaque, :ativa, :foto)
    end
end
