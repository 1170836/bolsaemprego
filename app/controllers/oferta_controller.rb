class OfertaController < ApplicationController

    before_action :logged_in_user, only: [:edit, :destroy, :create, :update, :mudar_estado]
    before_action :entidade_oferta, only: [:edit, :update, :mudar_estado]

    def index
        @ofertas = Ofertum.order(created_at: :desc)
        filtering_params(params).each do |key, value|
          @ofertas = @ofertas.public_send(key,value) if value.present?
        end
        @ofertas =  @ofertas.paginate(page:params[:page], per_page: 8)
    end

    def index_destaque
        @ofertas = Ofertum.destaque.order(created_at: :desc)
        filtering_params(params).each do |key, value|
          @ofertas = @ofertas.public_send(key,value) if value.present?
        end
        @ofertas =  @ofertas.paginate(page:params[:page], per_page: 8)
        render 'index'
    end

    def new
        @oferta = Ofertum.new
        @user = current_user
    end

    def show
        @oferta = Ofertum.find(params[:id])
        @candidatos = @oferta.candidatos.paginate(page:params[:page], per_page: 2)
        @ofertas_atividade = Ofertum.all.where('atividade_profissional like ? AND id != ?', @oferta.atividade_profissional, @oferta.id).limit(2)
    end

    def create
        @user = current_user
        @offer = @user.ofertum.build(oferta_params)
        if @offer.save
            flash[:success] = "Oferta guardada com Sucesso!"
            redirect_to oferta_path
        else
            render 'new'
        end
    end

    def edit
        @oferta = Ofertum.find(params[:id])
        @user = @oferta.user
    end

    def update
        @oferta = Ofertum.find(params[:id])
        @user = @oferta.user
        @oferta = @user.ofertum.find(params[:id])
        if @oferta.update_attributes(oferta_params)
          flash[:success] = "Oferta atualizada!"
          redirect_to @oferta
        else
          render 'edit'
        end
    end

    def mudar_estado
        @oferta = Ofertum.find(params[:id])
        @user = @oferta.user

        @oferta.mudar_estado
        flash[:success] = "Estado atualizado!"
        redirect_to @oferta
    end

    private

    def oferta_params
        params.require(:ofertum).permit(:titulo, :data_inicio, :data_fim, :descricao,
                                    :salario, :tipo_contrato, :atividade_profissional, :ativa, :foto,:destaque )
    end

    def filtering_params(params)
        params.slice(:search,:atividade_profissional)
    end

    def entidade_oferta
        @oferta = Ofertum.find(params[:id])
        redirect_to(root_url) unless current_user?(@oferta.user) || admin_user?(current_user)
      end
    
end
