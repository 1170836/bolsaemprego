class OfertaRelationshipsController < ApplicationController

    before_action :logged_in_user

    def create
        oferta = Ofertum.find(params[:oferta_id])
        current_user.follow_oferta(oferta)
        redirect_to oferta
    end

    def destroy
        oferta = OfertaRelationship.find(params[:id]).oferta
        current_user.unfollow_oferta(oferta)
        redirect_to oferta
    end
end
