class StaticController < ApplicationController
  def home
    @users = User.all.order(created_at: :desc)
    @entidades = @users.where(tipo: 'entidade').limit(3)
    @candidatos = @users.where(tipo: 'candidato').limit(3)

    @ofertas = Ofertum.ativa.order(created_at: :desc).limit(3);

    @noticias=Noticia.ativa.destaque.order(created_at: :desc).limit(3);
  end

  def about
  end

  def contacts
  end
end
