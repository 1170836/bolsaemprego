class UsersController < ApplicationController

  before_action :logged_in_user, only: [:edit, :update, :destroy,:edit_user,:edit_password]
  before_action :correct_user, only: [:edit, :update, :edit_user,:edit_password]
  before_action :admin_user, only: [:destroy]

  def new
    @user = User.new
  end

  def show
    @user = User.find(params[:id])
    @followers = @user.followers.paginate(page:params[:page], per_page:2)
    if @user.tipo == 'entidade'
        @ofertas = @user.ofertum.order(created_at: :desc)
        @ativas = @ofertas.ativa.paginate(page:params[:page], per_page:2)
        @historico = @ofertas.where(ativa: false).limit(2)
    else
      @candidaturas = @user.ofertas.ativa.paginate(page:params[:page], per_page:2)
    end
  end
  
  def create
    @user = User.new(user_params)
    @user.foto.attach(params[:user][:foto])
    if @user.save
        if admin_user?(current_user) && @user!=current_user
          flash[:success] = "Utilizador criado com sucesso"
          redirect_to backoffice_users_path
        else
          log_in @user
          flash[:success] = "Bem-vindo à Bolsa de Emprego"
          redirect_to @user
        end
    else
      if admin_user?(current_user)
        render 'backoffice/new'
      else
        render(params[:user][:tipo] == 'entidade' ? 'users/nova_entidade' : 'users/novo_candidato', :user => @user)
      end
    end
  end

  def novo_candidato
    @user = User.new
  end

  def nova_entidade
    @user = User.new
  end

  def index_candidatos
    @users = User.ativo.candidato.order(created_at: :desc)
    filtering_params(params).each do |key, value|
      @users = @users.public_send(key,value) if value.present?
    end
    @users = @users.paginate(page: params[:page], per_page:8)
  end

  def index_candidatos_destaque
    @users = User.ativo.candidato.destaque.order(created_at: :desc)
    filtering_params(params).each do |key, value|
      @users = @users.public_send(key,value) if value.present?
    end
    @users = @users.paginate(page: params[:page], per_page:8)
    render 'index_candidatos'
  end

  def index_entidades
    @users = User.ativo.entidade.order(created_at: :desc)
    filtering_params(params).each do |key, value|
      @users = @users.public_send(key,value) if value.present?
    end
    @users = @users.paginate(page: params[:page], per_page:8)
  end

  def index_entidades_destaque
    @users = User.ativo.entidade.destaque.order(created_at: :desc)
    filtering_params(params).each do |key, value|
      @users = @users.public_send(key,value) if value.present?
    end
    @users = @users.paginate(page: params[:page], per_page:8)
    render 'index_entidades'
  end

  def edit
    @user = User.find(params[:id])
  end

  def edit_user
    @user = User.find(params[:id])
  end

  def edit_password
    @user = User.find(params[:id])
  end

  def update
    @user = User.find(params[:id])
    
    if params[:user][:current_password].present?
      current_password = params[:user].delete(:current_password)
      user = @user.authenticate(current_password)

      if @user && user 
        if params[:user][:password] == params[:user][:password_confirmation] 
            if params[:user][:password].length>=6
              user.update_attribute(:password, params[:user][:password])
              flash[:success] = "Password Atualizada!"
              redirect_to admin_user?(current_user) ? backoffice_show_users_path(@user) : user_path(current_user.id)
            else
              flash[:danger] = "Password inválida"
              render 'edit_password'
            end

        else
          flash[:danger] = "Passwords não coincidem"
          render 'edit_password'
        end
      else
        flash[:danger] = "Password atual inválida"
        render 'edit_password'
      end

    else
      
      if @user.update_attributes(user_params)
        flash[:success] = "Perfil Atualizado!"
        redirect_to admin_user?(current_user)? backoffice_show_users_path(@user) : user_path(current_user.id)
      else
        render 'edit'
      end

    end

  end

  def destroy
    User.find(params[:id]).destroy
    flash[:success] = "Utilizador apagado!"
    redirect_to backoffice_users_path
  end


  private

  def user_params
    params.require(:user).permit(:name, :email, :password,
                                 :password_confirmation, :tipo, :morada, :codigo_postal, :localidade,
                                 :telefone, :telemovel, :pagina, :data_nascimento, :bilhete_identidade, :area_profissional, :apresentacao,
                                 :nivel_habilitacoes, :habilitacoes_literarias, :situacao_profissional, :experiencia_profissional,:activated,
                                 :foto,:cv,:newsletter,:destaque)
  end

  def filtering_params(params)
    params.slice(:search,:area_profissional,:situacao_profissional,:localidade)
  end
end
