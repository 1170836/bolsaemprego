class ApplicationMailer < ActionMailer::Base
  default from: 'bolsaemprego-helderpereira@outlook.pt'
  layout 'mailer'
end
