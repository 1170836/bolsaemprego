class UserMailer < ApplicationMailer

  # Subject can be set in your I18n file at config/locales/en.yml
  # with the following lookup:
  #
  #   en.user_mailer.newsletter_mailer.subject
  #
  def newsletter_mailer
    @users = User.criado_esta_semana
    @ofertas = Ofertum.criado_esta_semana
    emails = User.recebem_newsletter.collect(&:email).join(", ")
    mail(to: 'hjvp1999@hotmail.com', subject: "Newsletter Semanal")
  end

  def password_reset(user)
    @user = user
    mail to: user.email, subject: "Password reset"
  end
end
