class Noticia < ApplicationRecord
    has_one_attached :foto

    validates :titulo, presence: true
    validates :sumario, presence: true
    validates :descricao, presence: true

    scope :destaque, -> { where destaque: true}
    scope :ativa, -> { where ativa: true}
    scope :titulo, -> (t) {where('titulo like ?',"#{t}%")}

    def display_image
        foto.variant(resize_to_limit: [500, 500])
    end
end
