class OfertaRelationship < ApplicationRecord
    belongs_to :oferta, class_name: "Ofertum"
    belongs_to :candidato, class_name: "User"

    validates :oferta_id, presence: true
    validates :candidato_id, presence: true
end
