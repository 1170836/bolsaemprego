class Ofertum < ApplicationRecord
  belongs_to :user
  has_one_attached :foto

  has_many :oferta_passive_relationships, class_name: "OfertaRelationship",
           foreign_key: "oferta_id",
           dependent: :destroy

  has_many :candidatos, through: :oferta_passive_relationships, source: :candidato
  
  validates :user_id, presence: true
  validates :titulo, presence: true

  scope :ativa, -> { where ativa: true}
  scope :destaque, -> { where destaque: true}
  scope :search, -> (pesquisa) {where "titulo like ? ","%#{pesquisa}%"}
  scope :atividade_profissional, -> (area) { where atividade_profissional: area}
  scope :criado_esta_semana, -> {where("created_at >= ?",Time.zone.now - 7.days)}

  def display_image
    foto.variant(resize_to_limit: [500, 500])
  end

  def mudar_estado
    update_attribute(:ativa, !ativa)
  end
end
