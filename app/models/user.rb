class User < ApplicationRecord

    has_one_attached :foto
    has_one_attached :cv

    has_many :ofertum,dependent: :destroy

    #Seguir Entidades/Candidatos
    has_many :active_relationships, class_name: "Relationship",
    foreign_key: "follower_id",
    dependent: :destroy
    
    has_many :passive_relationships, class_name: "Relationship",
    foreign_key: "followed_id",
    
    dependent: :destroy
    
    has_many :following, through: :active_relationships, source: :followed
    has_many :followers, through: :passive_relationships, source: :follower

    #Candidatar a ofertas
    has_many :ofertas_active_relationships, class_name: "OfertaRelationship",
           foreign_key: "candidato_id",
           dependent: :destroy

    has_many :ofertas_passive_relationships, class_name: "OfertaRelationship",
            foreign_key: "oferta_id",
            dependent: :destroy

    has_many :ofertas, through: :ofertas_active_relationships, source: :oferta
    has_many :candidatos, through: :ofertas_passive_relationships, source: :candidato

    attr_accessor :remember_token, :activation_token, :reset_token

    before_save { email.downcase! }

    attribute :activated, :boolean, default: true
    attribute :destaque, :boolean, default: false
    attribute :newsletter, :boolean, default: true

    validates :name, presence: true, length: { maximum: 50 }
    
    VALID_EMAIL_REGEX = /\A[\w+\-.]+@[a-z\d\-.]+\.[a-z]+\z/i
    validates :email, presence: true, length: { maximum: 255 },
    format: { with: VALID_EMAIL_REGEX },
    uniqueness: true

    has_secure_password
    validates :password, presence: true, length: { minimum: 6 }, on: :create
    validates :password, length: { minimum: 6 }, allow_blank:true ,on: :update

    validates :morada, presence: true, allow_nil: true

    validates :codigo_postal, presence: true, allow_nil: true, zipcode: {country_code: :pt}
    validates :localidade, presence: true, allow_nil: true
    validates :telefone, presence: true , length: {is: 9}, numericality: true , allow_nil: true
    validates :telemovel, presence: true,numericality: true, length:  {is: 9}, allow_nil: true
    validates_date :data_nascimento, presence: true, allow_nil: true,
                    on_or_before: lambda { Date.current }, message: "- Data inválida"
    validates :bilhete_identidade, presence: true, allow_nil: true
    validates :apresentacao, presence: true, allow_nil: true
    validates :situacao_profissional, presence: true, allow_nil: true

    validates :foto, content_type: { in: %w[image/jpeg image/gif image/png], message: "- Formato inválido (Deve ser .jpeg, .gif ou .png)" },
                      size: { less_than: 5.megabytes, message: "- Demasiado grande (até 5MB)" }

    validates :cv, content_type: { in: %w[application/pdf], message: "- Formato inválido (Deve ser .pdf)" },
                      size: { less_than: 5.megabytes, message: "- Demasiado grande (até 5MB)" }

    #Scopes
    scope :estado, -> (estado) { where(activated: estado)}
    scope :ativo, -> { where(activated: true)}
    scope :destaque, -> { where(destaque: true)}
    scope :entidade, -> { where(tipo: 'entidade')}
    scope :candidato, -> { where(tipo: 'candidato')}
    scope :search, -> (query) {where("name like ? OR apresentacao like ?","%#{query}%","%#{query}%")}
    scope :localidade, -> (loc) { where localidade: loc}
    scope :situacao_profissional, -> (sit) { where situacao_profissional: sit}
    scope :area_profissional, -> (area) { where area_profissional: area}
    scope :tipo, -> (t) { where tipo: t}
    scope :recebem_newsletter, -> { where(newsletter: true)}
    scope :criado_esta_semana, -> {where("created_at >= ?",Time.zone.now - 7.days)}


    def remember
      self.remember_token = User.new_token
      update_attribute(:remember_digest, User.digest(remember_token))
    end

    def forget
      update_attribute(:remember_digest, nil)
    end

    def create_reset_digest
      self.reset_token = User.new_token
      update_columns(reset_digest: User.digest(reset_token), reset_sent_at: Time.zone.now)
    end
    
    # Sends password reset email.
    def send_password_reset_email
      UserMailer.password_reset(self).deliver_now
    end

    def password_reset_expired?
      reset_sent_at < 2.hours.ago
    end
    
    def authenticated?(remember_token)
      return false if remember_digest.nil?
      BCrypt::Password.new(remember_digest).is_password?(remember_token)
    end

    def display_image
      foto.variant(resize_to_limit: [500, 500])
    end

    # Follows a user.
    def follow(other_user)
      following << other_user
    end
  
    # Unfollows a user.
    def unfollow(other_user)
      following.delete(other_user)
    end
  
    # Returns true if the current user is following the other user.
    def following?(other_user)
      following.include?(other_user)
    end

    #offer
    def follow_oferta(oferta)
      ofertas_active_relationships.create(oferta_id: oferta.id)
    end

    def unfollow_oferta(oferta)
      ofertas_active_relationships.find_by(oferta_id: oferta.id).destroy
    end

    def candidato_oferta?(oferta)
      ofertas.include?(oferta)
    end

  


    def User.digest(string)
      cost = ActiveModel::SecurePassword.min_cost ? BCrypt::Engine::MIN_COST : BCrypt::Engine.cost
      BCrypt::Password.create(string, cost: cost)
    end

    def User.new_token
      SecureRandom.urlsafe_base64
    end

    class << self
        def digest(string)
          cost = ActiveModel::SecurePassword.min_cost ? BCrypt::Engine::MIN_COST :
              BCrypt::Engine.cost
          BCrypt::Password.create(string, cost: cost)
        end
    
        def new_token
          SecureRandom.urlsafe_base64
        end
    end
end
