Rails.application.routes.draw do
  root 'static#home'
  get  '/sobre',   to: 'static#about'
  get  '/contactos',   to: 'static#contacts'

  resources :users do
    member do
      get :following, :followers,:ofertas, :candidatos
    end
  end

  get 'signup', to: 'users#new'

  get '/entidades', to: 'users#index_entidades', as: 'index_entidades'
  get '/candidatos', to: 'users#index_candidatos', as: 'index_candidatos'
  get '/entidades_destaque', to: 'users#index_entidades_destaque', as: 'index_entidades_destaque'
  get '/candidatos_destaque', to: 'users#index_candidatos_destaque', as: 'index_candidatos_destaque'

  get '/novo_candidato' , to:'users#novo_candidato'
  post '/novo_candidato' , to:'users#create'
  
  get '/nova_entidade' , to:'users#nova_entidade'
  post '/nova_entidade' , to:'users#create'

  get '/login', to: 'sessions#new'
  post '/login', to: 'sessions#create'
  delete '/logout', to: 'sessions#destroy'

  patch '/users/:id/edit', to: 'users#update'
  get '/users/:id/edit_password', to: 'users#edit_password', as: 'edit_password'

  resources :oferta
  post '/oferta/new', to: 'oferta#create'
  get '/ofertas', to: 'oferta#index', as: 'index_ofertas'
  get '/ofertas_destaque', to: 'oferta#index_destaque', as: 'index_ofertas_destaque'
  put '/ofertas/:id/mudar_estado', to: 'oferta#mudar_estado', as: 'oferta_estado'

  resources :relationships, only: [:create, :destroy]
  resources :oferta_relationships, only: [:create, :destroy]
  
  resources :noticias
  post '/noticias/new', to: 'noticias#create'
  delete '/noticias/:id', to: 'noticias#destroy', as: 'noticias_delete'
  get '/noticias', to: 'noticias#index', as: 'index_noticias'

  get '/backoffice' ,to: 'backoffice#home'
  get '/backoffice/users', to: 'backoffice#index_users'
  get '/backoffice/noticias', to: 'backoffice#index_noticias'

  get '/backoffice/noticias/:id', to: 'backoffice#show_noticia', as: 'backoffice_show_noticia'
  get '/backoffice/users/:id', to: 'backoffice#show_users', as: 'backoffice_show_users'
  
  get '/backoffice/noticias/:id/edit', to:'backoffice#edit_noticias', as: 'backoffice_edit_noticias'
  patch '/backoffice/noticias/:id/edit', to:'noticias#update', as: 'backoffice_update_noticias'

  get '/backoffice/users/:id/edit_password', to:'backoffice#edit_user_password', as: 'backoffice_edit_user_password'
  get '/backoffice/users/:id/edit', to:'backoffice#edit_users', as: 'backoffice_edit_users'
  patch '/backoffice/users/:id/edit_password', to:'users#update'
  patch '/backoffice/users/:id/edit', to:'users#update'
  post '/backoffice/new', to:'users#create'


  resources :backoffice
  resources :newsletters
  resources :password_resets, only: [:new, :create, :edit, :update]
  

end
