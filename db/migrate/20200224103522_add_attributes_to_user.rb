class AddAttributesToUser < ActiveRecord::Migration[6.0]
  def change
    add_column :users, :morada, :string
    add_column :users, :codigo_postal, :string
    add_column :users, :localidade, :string
    add_column :users, :telefone, :integer
    add_column :users, :telemovel, :integer
    add_column :users, :pagina, :string
    add_column :users, :data_nascimento, :date
    add_column :users, :bilhete_identidade, :string
    add_column :users, :area_profissional, :string
    add_column :users, :apresentacao, :string
    add_column :users, :nivel_habilitacoes, :string
    add_column :users, :habilitacoes_literarias, :string
    add_column :users, :situacao_profissional, :string
    add_column :users, :experiencia_profissional, :string
    add_column :users, :tipo, :string
  end
end
