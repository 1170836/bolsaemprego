class MudarAtributosStringParaText < ActiveRecord::Migration[6.0]
  def change
    change_column :users, :morada, :text
    change_column :users, :apresentacao, :text
    change_column :users, :habilitacoes_literarias, :text
    change_column :users, :experiencia_profissional, :text
  end
end
