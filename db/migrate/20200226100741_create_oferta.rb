class CreateOferta < ActiveRecord::Migration[6.0]
  def change
    create_table :oferta do |t|
      t.string :titulo
      t.date :data_inicio
      t.date :data_fim
      t.text :descricao
      t.references :user, null: false, foreign_key: true
      t.string :atividade_profissional
      t.string :tipo_contrato
      t.string :salario
      t.boolean :ativa

      t.timestamps
    end
    add_index :oferta, [:user_id, :created_at]
  end
end
