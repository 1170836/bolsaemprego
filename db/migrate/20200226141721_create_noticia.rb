class CreateNoticia < ActiveRecord::Migration[6.0]
  def change
    create_table :noticia do |t|
      t.string :titulo
      t.date :data
      t.text :sumario
      t.text :descricao
      t.boolean :destaque
      t.boolean :ativa

      t.timestamps
    end
  end
end
