class CreateOfertaRelationships < ActiveRecord::Migration[6.0]
  def change
    create_table :oferta_relationships do |t|
      t.integer :oferta_id
      t.integer :candidato_id

      t.timestamps
    end
    add_index :oferta_relationships, :oferta_id
    add_index :oferta_relationships, :candidato_id
    add_index :oferta_relationships, [:oferta_id, :candidato_id], unique: true
  end
end
