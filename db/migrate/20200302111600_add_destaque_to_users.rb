class AddDestaqueToUsers < ActiveRecord::Migration[6.0]
  def change
    add_column :users, :destaque, :boolean
  end
end
