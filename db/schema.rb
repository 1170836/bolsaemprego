# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# This file is the source Rails uses to define your schema when running `rails
# db:schema:load`. When creating a new database, `rails db:schema:load` tends to
# be faster and is potentially less error prone than running all of your
# migrations from scratch. Old migrations may fail to apply correctly if those
# migrations use external dependencies or application code.
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 2020_03_06_090329) do

  create_table "active_storage_attachments", force: :cascade do |t|
    t.string "name", null: false
    t.string "record_type", null: false
    t.integer "record_id", null: false
    t.integer "blob_id", null: false
    t.datetime "created_at", null: false
    t.index ["blob_id"], name: "index_active_storage_attachments_on_blob_id"
    t.index ["record_type", "record_id", "name", "blob_id"], name: "index_active_storage_attachments_uniqueness", unique: true
  end

  create_table "active_storage_blobs", force: :cascade do |t|
    t.string "key", null: false
    t.string "filename", null: false
    t.string "content_type"
    t.text "metadata"
    t.bigint "byte_size", null: false
    t.string "checksum", null: false
    t.datetime "created_at", null: false
    t.index ["key"], name: "index_active_storage_blobs_on_key", unique: true
  end

  create_table "noticia", force: :cascade do |t|
    t.string "titulo"
    t.date "data"
    t.text "sumario"
    t.text "descricao"
    t.boolean "destaque"
    t.boolean "ativa"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
  end

  create_table "oferta", force: :cascade do |t|
    t.string "titulo"
    t.date "data_inicio"
    t.date "data_fim"
    t.text "descricao"
    t.integer "user_id", null: false
    t.string "atividade_profissional"
    t.string "tipo_contrato"
    t.string "salario"
    t.boolean "ativa"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.boolean "destaque"
    t.index ["user_id", "created_at"], name: "index_oferta_on_user_id_and_created_at"
    t.index ["user_id"], name: "index_oferta_on_user_id"
  end

  create_table "oferta_relationships", force: :cascade do |t|
    t.integer "oferta_id"
    t.integer "candidato_id"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["candidato_id"], name: "index_oferta_relationships_on_candidato_id"
    t.index ["oferta_id", "candidato_id"], name: "index_oferta_relationships_on_oferta_id_and_candidato_id", unique: true
    t.index ["oferta_id"], name: "index_oferta_relationships_on_oferta_id"
  end

  create_table "relationships", force: :cascade do |t|
    t.integer "follower_id"
    t.integer "followed_id"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["followed_id"], name: "index_relationships_on_followed_id"
    t.index ["follower_id", "followed_id"], name: "index_relationships_on_follower_id_and_followed_id", unique: true
    t.index ["follower_id"], name: "index_relationships_on_follower_id"
  end

  create_table "users", force: :cascade do |t|
    t.string "name"
    t.string "email"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.string "password_digest"
    t.text "morada"
    t.string "codigo_postal"
    t.string "localidade"
    t.integer "telefone"
    t.integer "telemovel"
    t.string "pagina"
    t.date "data_nascimento"
    t.string "bilhete_identidade"
    t.string "area_profissional"
    t.text "apresentacao"
    t.string "nivel_habilitacoes"
    t.text "habilitacoes_literarias"
    t.string "situacao_profissional"
    t.text "experiencia_profissional"
    t.string "tipo"
    t.string "remember_digest"
    t.boolean "activated"
    t.boolean "destaque"
    t.boolean "newsletter"
    t.string "reset_digest"
    t.datetime "reset_sent_at"
    t.index ["email"], name: "index_users_on_email", unique: true
  end

  add_foreign_key "active_storage_attachments", "active_storage_blobs", column: "blob_id"
  add_foreign_key "oferta", "users"
end
