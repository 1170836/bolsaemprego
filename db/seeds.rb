# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)

User.create!(name: 'Administrador',
             email: 'admin@admin.com',
             password:              'administrador',
             password_confirmation: 'administrador',
             tipo: 'admin',
             morada: 'Wiremaze',
             codigo_postal: '4065-000',
             localidade: 'Porto',
             telefone: '919999999',
             telemovel: '229999999',
             pagina: 'https://bolsa-emprego-wm-helder.herokuapp.com/',
             data_nascimento: Time.zone.now,
             bilhete_identidade: '123456789',
             area_profissional: 'Administrador',
             apresentacao: 'Administrador'
)

100.times do |n|
    
    email = Faker::Internet.email
    password = '123456789'
    pagina= Faker::Internet.url
    morada = Faker::Address.street_address
    codigo_postal = '1234-123'
    localidade = Faker::Address.city
    apresentacao = Faker::Lorem.paragraph
    experiencia_profissional = Faker::Lorem.paragraph
    habilitacoes_literarias = Faker::Lorem.paragraphs
    atividade_profissional = ['Engenharia', 'Letras', 'Saúde', 'Desporto'].shuffle.sample
    bilhete_identidade = Faker::IDNumber.valid

    telemovel = Faker::Number.number(digits: 9)
    telefone = Faker::Number.number(digits: 9)


    tipo = ['candidato','entidade'].shuffle.sample
    if(tipo == 'candidato')
        name  = Faker::Name.name
    else
        name = Faker::Company.name
    end
    nivel_habilitacoes = ['Secundário','Licenciatura','Mestrado','Doutoramento'].shuffle.sample
    situacao_profissional = ['Empregado','Desempregado'].shuffle.sample
    data_nascimento = Faker::Date.between(from: 50.years.ago, to: Date.today)
    ativo = [true,false].shuffle.sample
    destaque = [true,false].shuffle.sample
    newsletter = [true,false].shuffle.sample

    User.create!(name: name,
                 email: email,
                 password:              password,
                 password_confirmation: password,
                 tipo: tipo,
                 morada: morada,
                 codigo_postal: codigo_postal,
                 localidade: localidade,
                 telefone: telefone,
                 telemovel: telemovel,
                 pagina: pagina,
                 data_nascimento: data_nascimento,
                 bilhete_identidade: bilhete_identidade,
                 area_profissional: atividade_profissional,
                 apresentacao: apresentacao,
                 nivel_habilitacoes: nivel_habilitacoes,
                 experiencia_profissional: experiencia_profissional,
                 habilitacoes_literarias: habilitacoes_literarias,
                 situacao_profissional: situacao_profissional,
                 activated:ativo,
                 destaque:destaque,
                 newsletter:newsletter)
  end

  20.times do |n|
    titulo  = Faker::Lorem.sentence
    data = Faker::Date.between(from: 50.years.ago, to: Date.today)
    sumario = Faker::Lorem.sentence
    descricao = Faker::Lorem.paragraphs
    destaque= [true,false].shuffle.sample
    ativa = [true,false].shuffle.sample
    Noticia.create!(titulo: titulo,
                 data: data,
                 sumario:sumario,
                 descricao:descricao,
                 destaque: destaque,
                 ativa: ativa)
  end

  users = User.entidade
    users.each { |user| 
        5.times do
                titulo = Faker::Lorem.sentence
                descricao = Faker::Lorem.sentence
                atividade_profissional = ['Engenharia', 'Letras', 'Saúde', 'Desporto'].shuffle.sample
                tipo_contrato = ['Full-Time','Part-Time'].shuffle.sample
                salario = ['Salário mínimo','600€-1500€','1500€-2000€','2000€-'].shuffle.sample
                data_inicio = Faker::Date.between(from: 1.years.ago, to: 1.month.ago)
                data_fim = Faker::Date.between(from: 1.month.ago, to: Date.today.end_of_month)
                destaque= [true,false].shuffle.sample
                ativa = [true,false].shuffle.sample

                user.ofertum.create!(titulo:titulo, 
                                            descricao: descricao,
                                            data_inicio: data_inicio, 
                                            data_fim: data_fim,
                                            ativa: ativa,
                                            destaque:destaque, 
                                            atividade_profissional: atividade_profissional,
                                            tipo_contrato: tipo_contrato, 
                                            salario: salario)
        end
    }
  


users = User.all
user  = User.candidato.first
following = User.candidato
followers = User.entidade
following.each { |followed| user.follow(followed) if user.tipo != followed.tipo }
followers.each { |follower| follower.follow(user) if user.tipo != follower.tipo }

candidatos = User.candidato
ofertas = Ofertum.all
entidades = User.entidade

candidatos.each{ |cand|

    5.times do
        of=ofertas.shuffle.sample
        cand.follow_oferta(of) unless cand.candidato_oferta?(of)
    end

    5.times do
        ent = entidades.shuffle.sample
        cand.follow(ent) unless cand.following?(ent)
    end
}

entidades.each{ |ent|

    5.times do
        cand = candidatos.shuffle.sample
        ent.follow(cand) unless ent.following?(cand)
    end
}

